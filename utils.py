from math import ceil

def take(l, n):
    ret = l[:n]
    l[:] = l[n:]
    return ret

def windows(l, size=4):
    idx = 0
    while idx < len(l):
        if idx + size < len(l):
            yield l[idx:idx+size], idx
            idx += 1
        else:
            yield l[idx:], idx
            break

def bitstream(b, size=8):
    idx = 0
    while (idx + size) / 8 <= len(b):
        first_byte = idx // 8
        last_byte  = ceil((idx + size) / 8)
        num = read_int(b, first_byte, last_byte)
        shift = (last_byte - first_byte) * 8 - (idx % 8) - size
        yield num >> shift & 2**size - 1
        idx += size

def read_int(data, *args, **kwargs):
    if not args:         start, stop = None, None
    elif len(args) == 1: start, stop = None, args[0]
    elif len(args) == 2: start, stop = args

    return int.from_bytes(data[start:stop],
            byteorder=kwargs.get('byteorder') or 'big',
            signed=kwargs.get('signed') or False)

def read_ascii(data, *args):
    if not args:         start, stop = None, None
    elif len(args) == 1: start, stop = None, args[0]
    elif len(args) == 2: start, stop = args

    try:
        return data[start:stop].decode('ascii')
    except UnicodeDecodeError:
        return None

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate
