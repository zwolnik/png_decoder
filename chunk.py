from utils import take, read_int

class Chunk:
    def __init__(self, head=bytes(4), data=bytes(), crc32=bytes(4)):
        self.header = head
        self.data = data
        self.crc32 = crc32

    @classmethod
    def from_data(cls, data):
        length = read_int(take(data, 4), 4)
        h = bytes(take(data, 4))
        d = bytes(take(data, length))
        c = bytes(take(data, 4))
        return cls(head=h, data=d, crc32=c)

    def ancillary(self):
        return (self.header[0] & 0b0010_0000) >> 5

    def private(self):
        return (self.header[1] & 0b0010_0000) >> 5

    def reserved(self):
        # bit should always be 1
        return (self.header[2] & 0b0010_0000) >> 5

    def safe_to_copy(self):
        return (self.header[3] & 0b0010_0000) >> 5
