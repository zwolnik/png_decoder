#!/bin/python

""" Based on http://www.libpng.org/pub/png/spec/iso """

from chunk import Chunk
from utils import take, windows, read_int, read_ascii, bitstream, static_vars

from enum import Enum, auto
from copy import deepcopy
import itertools
import math
import zlib

import numpy as np
from PIL import Image
from matplotlib import pyplot as plt

basic = 'static/output-onlinepngtools.png'
dices = 'static/dices.png'
with_chroma_chunk = 'static/chroma_chunk.png'

grayscale_2bit  = 'static/2_bit_grayscale.png'
grayscale_4bit  = 'static/4_bit_grayscale.png'
grayscale_8bit  = 'static/8_bit_grayscale.png'
grayscale_16bit = 'static/16_bit_grayscale.png'

class ColorType(Enum):
    """ RGBA """
    TrueColorAlpha = 6
    """ GrA """
    GrayScaleAlpha = 4
    """ RGB """
    TrueColor = 2
    """ Gr """
    GrayScale = 0
    """ Indexed in a palette """
    IndexedColor = 3

    def channels(self):
        if self == ColorType.TrueColorAlpha:   return 4
        elif self == ColorType.TrueColor:      return 3
        elif self == ColorType.GrayScaleAlpha: return 2
        else:                                  return 1

    def __str__(self):
        if   self == ColorType.TrueColorAlpha: return 'RGBA'
        elif self == ColorType.GrayScaleAlpha: return 'LA'
        elif self == ColorType.TrueColor:      return 'RGB'
        elif self == ColorType.GrayScale:      return 'L'
        elif self == ColorType.IndexedColor:   return 'P'

class InterlaceMethod(Enum):
    NoInterlace = 0
    Adam7 = 1

class AncillaryType(Enum):
    """ Solid background colour to be used when presenting the image if no better option is available. """
    BackgroundColor = auto()
    """ Gamma characteristic of the image with respect to the desired output intensity, and chromaticity characteristics of the RGB values used in the image. """
    GammaAndChromacity = auto()
    """ Description of the colour space (in the form of an International Color Consortium (ICC) profile) to which the samples in the image conform. """
    IccProfile = auto()
    """ Estimates of how frequently the image uses each palette entry. """
    ImageHistogram = auto()
    """ Intended pixel size and aspect ratio to be used in presenting the PNG image. """
    PhysicalPxDim = auto()
    """ The number of bits that are significant in the samples. """
    SignificantBits = auto()
    """ A rendering intent (as defined by the International Color Consortium) and an indication that the image samples conform to this colour space. """
    sRGBColorSpace = auto()
    """ A reduced palette that may be used when the display device is not capable of displaying the full range of colours in the image. """
    SuggestedPalette = auto()
    """ Textual information (which may be compressed) associated with the image. """
    TextualData = auto()
    """ The time when the PNG image was last modified. """
    Time = auto()
    """ Alpha information that allows the reference image to be reconstructed when the alpha channel is not retained in the PNG image. """
    Transparency = auto()

class PngReader:
    def __init__(self):
        self.png = None
        self.chunks = []
        self.trash_data = []

    def check_header(self, data):
        return take(data, 8) == bytes([137, 80, 78, 71, 13, 10, 26, 10])

    def parse_chunk(self, data, idx):
        handle = {
        # Critical chunks
            # image header, which is the first chunk in a PNG datastream.
            'IHDR': self._IHDR,
            # palette table associated with indexed PNG images.
            'PLTE': self._PLTE,
            # image data chunks.
            'IDAT': lambda c: self.png.data.extend(c.data),
            # image trailer, which is the last chunk in a PNG datastream.
            'IEND': lambda _: self.png.decode(),
        # Ancillary chunks
            # Transparency information
            'tRNS': lambda _: None,
            # Colour space information
            'cHRM': lambda _: None,
            'gAMA': lambda _: None,
            'iCCP': lambda _: None,
            'sBIT': lambda _: None,
            'sRGB': lambda _: None,
            # Textual information
            'iTXt': lambda _: None,
            'tEXt': lambda _: None,
            'zTXt': lambda _: None,
            # Miscellaneous information
            'bKGD': lambda _: None,
            'hIST': lambda _: None,
            'pHYs': lambda _: None,
            'sPLT': lambda _: None,
            # Time information
            'tIME': lambda _: None,
        }
        chunk_type = read_ascii(data, idx, idx+4)
        if chunk_type in handle.keys():
            if idx != 4: self.trash_data.append(take(data, idx-4))
            chunk = Chunk.from_data(data)
            print(chunk.header)
            self.validate_order(chunk.header)
            handle[chunk_type](chunk)
            self.chunks.append(chunk)
            return True
        return False

    def _IHDR(self, chunk):
        if self.chunks:
            raise ValueError("IHDR shall be first chunk in PNG")

        width  = read_int(chunk.data, 4)
        height = read_int(chunk.data, 4, 8)
        if width == 0 or height == 0:
            raise ValueError('Width and height must be non 0 unsigned')

        color_type = ColorType(read_int(chunk.data, 9, 10))
        bit_depth = read_int(chunk.data, 8, 9)
        if color_type is ColorType.GrayScale:      allowed = [1, 2, 4, 8, 16]
        elif color_type is ColorType.IndexedColor: allowed = [1, 2, 4, 8]
        elif color_type in [ColorType.TrueColor,
                            ColorType.GrayScaleAlpha,
                            ColorType.TrueColorAlpha]: allowed = [8, 16]
        if bit_depth not in allowed:
            raise ValueError(f'Bit depth of value {bit_depth} not allowed for Color type {color_type.name}')

        compression_method = read_int(chunk.data, 10, 11)
        if compression_method != 0:
            raise ValueError(f'Compression method `{compression_method}` is not defined in PNG standard')

        filter_method = read_int(chunk.data, 11, 12)
        if filter_method != 0:
            raise ValueError(f'Filter method `{filter_method}` is not defined in PNG standard')

        interlace_method = InterlaceMethod(read_int(chunk.data, 12, 13))

        self.png = Png(
            width,
            height,
            bit_depth,
            color_type,
            compression_method,
            filter_method,
            interlace_method)

        print(self.png)

    def _PLTE(self, chunk):
        pass

    def validate_order(self, header):
        def first():
            if self.png:
                raise ValueError(f'Chunk {header} should appear first')
            return True

        def after_ihdr():
            if not self.png:
                raise ValueError(f'Chunk {header} should appear after IHDR')
            return True

        def before_plte():
            if list(filter(lambda c: c.header == b'PLTE', self.chunks)):
                raise ValueError(f'Chunk {header} should appear before PLTE')
            return True

        def before_idat():
            if list(filter(lambda c: c.header == b'IDAT', self.chunks)):
                raise ValueError(f'Chunk {header} should appear before IDAT')
            return True

        def mutually_exclusive(other):
            if list(filter(lambda c: c.header == other, self.chunks)):
                raise ValueError(f'Chunk {header} should not appear if {other} is present')
            return True

        @static_vars(started=False)
        def consecutive_idat():
            if consecutive_idat.started and self.chunks[-1].header != b'IDAT':
                raise ValueError(f'Chunk IDAT appeared in non consecutive manner')
            elif not consecutive_idat.started:
                consecutive_idat.started = True
            return True

        def validate_chunks_before_plte():
            not_allowed_before_plte = list(filter(lambda c: c.header in [b'bKGD', b'hIST', b'tRNS'], self.chunks))
            if not_allowed_before_plte:
                raise ValueError(f'Found {[i.header for i in not_allowed_before_plte]} that should only appear after PLTE')
            return True

        validate = {
            b'IHDR': lambda: first(),
            b'PLTE': lambda: after_ihdr() and before_idat() and validate_chunks_before_plte(),
            b'IDAT': lambda: after_ihdr() and consecutive_idat(),
            b'IEND': lambda: after_ihdr(),
            b'tRNS': lambda: after_ihdr() and before_idat(),
            b'cHRM': lambda: after_ihdr() and before_plte() and before_idat(),
            b'gAMA': lambda: after_ihdr() and before_plte() and before_idat(),
            b'iCCP': lambda: after_ihdr() and before_plte() and before_idat() and mutually_exclusive(b'sRGB'),
            b'sBIT': lambda: after_ihdr() and before_plte() and before_idat(),
            b'sRGB': lambda: after_ihdr() and before_plte() and before_idat() and mutually_exclusive(b'iCCP'),
            b'iTXt': lambda: after_ihdr(),
            b'tEXt': lambda: after_ihdr(),
            b'zTXt': lambda: after_ihdr(),
            b'bKGD': lambda: after_ihdr() and before_idat(),
            b'hIST': lambda: after_ihdr() and before_idat(),
            b'pHYs': lambda: after_ihdr() and before_idat(),
            b'sPLT': lambda: after_ihdr() and before_idat(),
            b'tIME': lambda: after_ihdr(),
        }
        validate[header]()


    @classmethod
    def decode_file(cls, filename):
        reader = cls()
        with open(filename, 'rb') as f:
            data = bytearray(f.read())
        if not reader.check_header(data):
            raise ValueError("Data doesn't contain proper header")

        while data:
            it = windows(data)
            for s, idx in it:
                if reader.parse_chunk(data, idx):
                    break

        return reader.png

class Png:
    def __init__(self, *args):
        w, h, bd, c, cm, fm, im = args
        self.width = w
        self.height = h
        self.bit_depth = bd
        self.color_type = c
        self.compression_method = cm
        self.filter_method = fm
        self.interlace_method = im
        self.data = bytearray()
        self.sample_depth = 8 if self.color_type is ColorType.IndexedColor else self.bit_depth
        self.dtype = np.uint8 if self.bit_depth <= 8 else np.uint16

    def decode(self):
        # For implementing zlib from hand
        #compression_flags = chunk.data[0]
        #additional_flags  = chunk.data[1]
        #checksum = chunk.data[-4:]
        # Decompress
        data = np.array(np.frombuffer(zlib.decompress(self.data), dtype=np.uint8))
        # Make scanlines
        data = data.reshape((self.height, -1))
        # Revert filtering
        filters, data = np.split(data, [1], axis=1)
        self.unfilter(filters.flatten(), data)
        # Extract samples
        if not self.bit_depth == 8:
            data_payload = data.tobytes()
            h, w = self.height, self.width * self.color_type.channels()
            extracted = np.zeros((h, w), dtype=self.dtype)
            stream = bitstream(data_payload, size=self.bit_depth)
            for pos in itertools.product(range(h), range(w)):
                extracted[pos] = next(stream)
        else:
            extracted = data
        # Extract pixels
        if self.color_type.channels() != 1:
            extracted = extracted.reshape((self.height, self.width, self.color_type.channels()))
        # Save
        self.data = extracted

    def unfilter(self, filters, data):
        def a(pos):
            y, x = pos
            new_x = x - (self.color_type.channels() * math.ceil(self.sample_depth / 8))
            return int(data[y, new_x]) if new_x >= 0 else 0

        def b(pos):
            y, x = pos
            return int(data[y-1, x]) if y else 0

        def c(pos):
            y, x = pos
            return a((y-1, x)) if y else 0

        def paeth(pos):
            p = a(pos) + b(pos) - c(pos)
            pa = abs(p - a(pos))
            pb = abs(p - b(pos))
            pc = abs(p - c(pos))
            if pa <= pb and pa <= pc: return a(pos)
            elif pb <= pc:            return b(pos)
            else:                     return c(pos)

        defil = {
            0: lambda pos: (data[pos]) % 256,
            1: lambda pos: (data[pos] + a(pos)) % 256,
            2: lambda pos: (data[pos] + b(pos)) % 256,
            3: lambda pos: (data[pos] + (a(pos) + b(pos)) // 2) % 256,
            4: lambda pos: (data[pos] + paeth(pos)) % 256
        }

        for pos in itertools.product(range(data.shape[0]), range(data.shape[1])):
            data[pos] = defil[filters[pos[0]]](pos)

    def to_grayscale(self):
        if self.color_type == ColorType.GrayScale:
            return self

        ret = deepcopy(self)
        if ret.color_type in [ColorType.TrueColor, ColorType.TrueColorAlpha]:
            ret.data = np.dot(ret.data[..., :3], [0.2989, 0.5870, 0.1140])
        elif ret.color_type == ColorType.GrayScaleAlpha:
            ret.data = ret.data[..., 0]
        else:
            raise TypeError("Cannot convert indexed image into grayscale")
        ret.color_type = ColorType.GrayScale
        return ret

    def show_fft(self):
        gray = self.to_grayscale()
        fft = np.fft.fft2(gray.data)
        plt.imshow(np.log(np.abs(np.fft.fftshift(fft))**2))
        plt.show()

    def __str__(self):
        return f''' Png:
    width:              {self.width}
    height:             {self.height}
    bit depth:          {self.bit_depth}
    sample depth:       {self.sample_depth}
    color type:         {self.color_type.name}
    filter method:      {self.filter_method}
    interlace method:   {self.interlace_method.name}
    compression method: {self.compression_method}'''

    def show(self):
        data = np.array(np.interp(self.data, [0, 2**self.bit_depth], [0, 256]), dtype=self.dtype)
        img = Image.fromarray(data, str(self.color_type))
        img.show(command='feh')

if __name__ == '__main__':
    import sys
    name = sys.argv[1]
    png = PngReader.decode_file(name)
    png.show()
    png.show_fft()

